import initStoryshots from "@storybook/addon-storyshots";
import { imageSnapshot } from "@storybook/addon-storyshots-puppeteer";
import path from "path";
import { startStorybook, stopStorybook } from "../test-utils/storybook-service";

const sizes = ["1280x1024", "1024x768", "800x600", "320x640"];

beforeAll(async () => {
  await startStorybook();
}, 60000);

afterAll(async () => {
  await stopStorybook().catch(console.error);
});

sizes.forEach((size) => {
  describe("with size " + size, () => {
    const [width, height] = size.split("x").map(Number);

    initStoryshots({
      suite: "Image storyshots",
      test: imageSnapshot({
        storybookUrl: "http://localhost:6006",
        beforeScreenshot: async (page, options) => {
          await page.setViewport({ width, height });
          if (
            options.context.parameters &&
            options.context.parameters.puppeteerTest
          ) {
            await options.context.parameters.puppeteerTest(page);
          }
        },
        getMatchOptions: (options) => {
          const directory = path.join(
            "__image_snapshots_puppeteer__",
            options.context.kind,
            options.context.story
          );
          return {
            customSnapshotsDir: directory,
          };
        },
      }),
    });
  });
});
