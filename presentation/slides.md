---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
---

# **Visual regression testing**

mit Storybook

*Nils Knappmeier*

---

# Motivation

React snapshot tests (example)

```js
exports[`changes the class when hovered 1`] = `
<a
  className="normal"
  href="http://www.facebook.com"
  onMouseEnter={[Function]}
  onMouseLeave={[Function]}
>
  Facebook
</a>
`;

```

---

```js
exports[`App increments the counter 1`] = `
<div>
  <h1>
    My Counter
  </h1>
  <div>
    <p>
      0
    </p>
  </div>
  <button
    onClick={[Function]}
    type="button"
  >
    Increment
  </button>
  <button
    onClick={[Function]}
    type="button"
  >
    Decrement
  </button>
</div>
`;
```

<!-- 
No CSS, no images, hard to verify
-->

--- 

# End-to-end test

* Complete page
* Simulate actions
* Take screenshots

<!--
We did this, but it is slow, fragile and complex
-->

--- 
# Storybook

Single Components

---


```jsx
import React from "react";
import styled from "styled-components";

const StListWrapper = styled.div`
  border: 2px solid red;
  background-color: yellow;
`;

export function MyList({ elements, title }) {
  if (elements.length === 0) {
    return <StListWrapper><b>The list is empty</b></StListWrapper>;
  }
  return (
    <StListWrapper>
      <ul className={'mylist'}>
        {elements.map((element) => (
          <li key={element.name}>- {element.name}</li>
        ))}
      </ul>
    </StListWrapper>
  );
}
```

---

# Story: Empty 

![bg right:40% auto](./images/storyshots-puppeteer-test-js-with-size-320-x-640-image-storyshots-my-list-empty-1-snap.png)

```jsx
const emptyList = [];
export const Empty = () => <MyList elements={emptyList} />;
```

---

# Story: SingleElement 

![bg right:40% auto](./images/storyshots-puppeteer-test-js-with-size-320-x-640-image-storyshots-my-list-single-element-1-snap.png)

```jsx
const singleElementList = [
  {
    name: "Test",
  },
];
export const SingleElement = () => <MyList elements={singleElementList} />;
```

---

# Story: Multiple Elements 

![bg right:40% auto](./images/storyshots-puppeteer-test-js-with-size-320-x-640-image-storyshots-my-list-multi-element-1-snap.png)

```js
const multiElementList = [
  {
    name: "First Element",
  },
  {
    name: "Second Element",
  },
];
export const MultiElement = () => <MyList elements={multiElementList} />;
```

---

![bg left:40% cover](./images/nicolas-ladino-silva-o2DVsV2PnHE-unsplash.jpg)

# Idea: Take a photo

* Headless Browser
* Selenium Hub
* Selenium as a service

---

# Spot the difference

![](./images/storyshots-puppeteer-test-js-with-size-320-x-640-image-storyshots-my-list-multi-element-1-diff.png)

---

# Requirements

* Must work on local-machine
* Must work in CI/CD
* Must work in combination of both

![bg left:40% fit](./images/environments.png)

---

# Solutions

* Chromatic (paid service)
* Storyshot
  * Jest-Snapshots by default :-(
  * Puppeteer
  * Selenium
  * Selenium as a Service

---

# Puppeteer

storyshot.test.js

```js
import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';
 
initStoryshots({ suite: 'Image storyshots', test: imageSnapshot() });
```

-> __image_snapshots__

---

# Puppeteer (multiple resolutions)

-> show IDE

---

# Puppeteer (gitlab-ci.yml)

```yaml
test:
  image: alekzonder/puppeteer:latest
  script:
    - yarn install
    - # start storybook server
    - yarn test
  artifacts:
    name: snapshots
    paths:
      - __image_snapshots_puppeteer__
    when: on_failure
```
---

# Puppeteer (fails)

![fit](./images/diff-gitlab-ci.png)

* Different fonts
* Anti-aliasing
* Solution? 
  * Set comparison threshold?

---

# Selenium Grid as docker image

* Official
  * selenium/hub
  * selenium/node-chrome
  * selenium/node-firefox
  * selenium/node-opera

--- 

![bg fit](./images/selenium-official.png)
 
---

![bg fit](./images/selenium-official-problems.png)

---

# Trying to make it work

* Compile statically, mount volumes
* Run with host-network
* Does not work in CI/CD

---
# All-in-one

* [elgalu/selenium](https://github.com/elgalu/docker-selenium)
  * includes chrome and firefox
  * includes VNC-server

---

![bg fit](./images/reverse-tunnel.png)

---


# Reverse tunnel

* dockerhub: `knappmeier/selenium-with-tunnel`
* chisel (tcp tunnel in Go)
* npm: `child-service` (start and wait for process)

---

# Demo

---

# Other way

* Setup as infrastructure
* official selenium
* Add ssh-tunnel to hub

* Works, but only in intranet

---

# Other browsers

![bg fit](./images/main-desktop-browser-logos.png)

---

# Selenium as a service

* Browserstack
* Saucelabs
* Testingbot

## 300$/month for 2 concurrent builds

* Similar integration

